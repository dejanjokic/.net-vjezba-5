﻿using System;
using System.Linq;
using System.Web.Mvc;
using Vjezba.Web.Models;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("komp")]
    public class CompanyController : Controller
    {
        public ActionResult Index(string query)
        {
            //Metoda All() vraća objekt tipa IQueryable (takav objekt je moguce dalje "napadati" s LINQ upitima)
            var companies = MockCompanyRepository.GetInstance().All()
                .ToList();

            if (query != null)
                companies = companies.Where(p => p.Name.ToLower().Contains(query)).ToList();

            //Naziv view-a se automatski skuzi iz naziva akcije controller-a, dovoljno je proslijediti samo model
            return View(companies);
        }

        [HttpPost]
        public ActionResult Index(string queryName, string queryAddress)
        {
            var companies = MockCompanyRepository.GetInstance().All()
                .ToList();

            if (queryName != null || queryAddress != null)
                companies = companies.Where(p => p.Name.ToLower().Contains(queryName)).Where(p => p.Address.ToLower().Contains(queryAddress)).ToList();

            return View(companies);
        }

        public ActionResult Details(int? id = null)
        {
            if (id == null)
                return View();

            var model = MockCompanyRepository.GetInstance().FindByID(id.Value);

            return View(model);
        }

        [Route("pretraga/{q:alpha:length(2,5)}")]
        public ActionResult Search(string q)
        {
            //Kada se bude radilo s bazom podataka, ova metoda još neće "otići" u bazu po podatke
            var query = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(p => p.Name.Contains(q));

            //Kada se bude radilo s bazom podataka, tek ova metoda bi generirala ispravni SQL i izvršila upit na bazi
            //te vratila samo potrebne rezultate
            var model = query.ToList();

            return View("Index", model);
        }

        [HttpPost]
        public ActionResult AdvancedSearch(CompanyFilterModel model)
        {
            var companies = MockCompanyRepository.GetInstance().All().
                ToList();

            companies = companies.
                Where(com => (model.imeKompanije != null && model.imeKompanije.Length > 0) ? com.Name.ToLower().Contains(model.imeKompanije) : true).
                Where(com => (model.adresaKompanije != null && model.adresaKompanije.Length > 0) ? com.Address.ToLower().Contains(model.adresaKompanije) : true).
                Where(com => (model.emailKompanije != null && model.emailKompanije.Length > 0) ? com.Email.ToLower().Contains(model.emailKompanije) : true).
                Where(com => (model.gradKompanije != null && model.gradKompanije.Length > 0) ? com.City.Name.ToLower().Contains(model.gradKompanije) : true).
                ToList();

            return View("Index", companies);
        }

        public ActionResult Create()
        {
            return View("Create", new Company());
        }

        [HttpPost]
        public ActionResult Create(Company model)
        {
            int id = MockCompanyRepository.GetInstance().All().Max(comp => comp.ID);
            DateTime date = DateTime.Now;
            model.DateFrom = date;
            model.ID = id + 1;
            MockCompanyRepository.GetInstance().InsertOrUpdate(model);
            return View("Index", MockCompanyRepository.GetInstance().All().ToList());
        }
    }
}