﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vjezba.Web.Models
{
    public class CompanyFilterModel
    {
        public string imeKompanije { get; set; }
        public string adresaKompanije { get; set; }
        public string emailKompanije { get; set; }
        public string gradKompanije { get; set; }
    }
}